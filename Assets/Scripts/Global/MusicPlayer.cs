﻿using UnityEngine;
using System.Collections;

public class MusicPlayer : MonoBehaviour
{
    private static bool musicStarted;

    // Use this for initialization
    void Start()
    {
        AudioSource audioSource = GetComponent<AudioSource>();
        // music started once time and she's repeated.
        if ( !musicStarted )
        {
            DontDestroyOnLoad(gameObject);
            audioSource.Play();
            musicStarted = true;
        } else
        {
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
