﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ActionsManager : MonoBehaviour
{
    private const string MENU_SCENE_NAME = "scene-menu";
    private const string GAME_SCENE_NAME = "scene-game";

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void StartMenuScene()
    {
        // load menu scene
        SceneManager.LoadScene(MENU_SCENE_NAME);
    }

    public void StartGameScene()
    {
        // load game scene
        SceneManager.LoadScene(GAME_SCENE_NAME);
    }
}
