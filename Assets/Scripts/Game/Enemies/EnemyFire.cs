﻿using UnityEngine;
using System.Collections;
using System;
using UnityEditor;


public class EnemyFire : MonoBehaviour, ScheduledTask
{
    private const float FIRE_ANGLE = 20;
    private const string ENEMY_BULLET = "red_bullet";
    private const float BULLET_SPEED = 5;

    // Use this for initialization
    void Start()
    {
        // schedule object to fire at cyclic frequence
        TaskScheduler taskScheduler = TaskScheduler.GetInstance();
        taskScheduler.ScheduleTask(80, this);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ExecuteTask()
    {
        // fire on player only if upper than him.
        Vector3 shipPosition = GameObject.Find("ship").transform.position;
        if (shipPosition.y < transform.position.y )
        {
            fire();
        }
        
    }

    public void fire()
    {
        // creates an new bullet at enemy position
        Vector3 enemyPosition = gameObject.transform.position;
        GameObject bullet = Instantiate(Resources.Load(ENEMY_BULLET), enemyPosition, Quaternion.identity) as GameObject;

        // modify bullet's velocity and rotation to hit the ship
        Vector3 shipPosition = GameObject.Find("ship").transform.position;

        float rotation = angle(enemyPosition, shipPosition);
        rotation = UnityEngine.Random.Range(rotation - FIRE_ANGLE, rotation + FIRE_ANGLE);
        bullet.transform.Rotate(new Vector3(0, 0, (rotation-90)%90));

        // compute bullet velocity
        bullet.GetComponent<Rigidbody2D>().velocity = Quaternion.AngleAxis(rotation, Vector3.forward) * Vector3.right * -BULLET_SPEED;
        
    }

    private float angle( Vector3 position1, Vector3 position2 )
    {
        return (float)((float)(Math.Atan2(position1.y - position2.y, position1.x - position2.x) * 180) / Math.PI);
    }
}

