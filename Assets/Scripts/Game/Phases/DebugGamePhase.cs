﻿public class DebugGamePhase : GamePhase
{
    private int currentWave;

    public DebugGamePhase(GameManager gameManager) : base(gameManager, "DEBUG")
    {
    }

    public override void Start()
    {
        this.currentWave = 0;
        
    }

    public override void Stop()
    {
        
    }

    public override void Update()
    {
        if (currentWave == 1 && this.allEnemiesKilled())
        {
            finish();
        }
        else if (currentWave == 0)
        {
            this.startFirstWave();
        }
    }

    private void startFirstWave()
    {
        this.currentWave = 1;
        createEnemies(EnemyType.SIMPLE_ENEMY, 1);
    }
}
