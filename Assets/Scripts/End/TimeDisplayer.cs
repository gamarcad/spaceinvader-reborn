﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class TimeDisplayer : MonoBehaviour
{
    private const string BEST_TIME_TEXT = "best_time";
    private const string LAST_TIME_TEXT = "last_time";
    // Use this for initialization
    void Start()
    {

        TimeStorage storage = TimeStorage.GetInstance();
        Text bestTime = GameObject.Find(BEST_TIME_TEXT).GetComponent<Text>();
        Text lastTime = GameObject.Find(LAST_TIME_TEXT).GetComponent<Text>();
        bestTime.text = TimeStorage.FormatTime(storage.GetBestTime());
        lastTime.text = TimeStorage.FormatTime(storage.GetLastTime());
        
    }

    // Update is called once per frame
    void Update()
    {

    }
}
