﻿using UnityEngine;
using System.Collections;

public class LifeBonus : AbstractBonusBehavior
{
    protected override void OnBonusClaimed()
    {
        ShipLife shipLife = GameObject.Find("ship").GetComponent<ShipLife>();
        shipLife.increaseLife();
    }
}
