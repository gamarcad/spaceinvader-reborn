﻿using UnityEngine;
using System.Collections;

public abstract class AbstractBonusBehavior : MonoBehaviour
{
    private Vector3 leftBottomPosition;
    

    // Use this for initialization
    void Start()
    {
        this.leftBottomPosition = Camera.main.ViewportToWorldPoint(new Vector3(0, 0));
        this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0, -2);
        
    }

    // Update is called once per frame
    void Update()
    {
        // destroy bonus if falling under screen
        // means that player doesn't have taken bonus
        if ( this.transform.position.y < this.leftBottomPosition.y )
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // 
        if ( collision.tag == Tags.PLAYER )
        {
            this.OnBonusClaimed();
            Destroy(gameObject);
        }
    }

    protected abstract void OnBonusClaimed();
}
