﻿using UnityEngine;
using System.Collections;

public class BulletBehavior : MonoBehaviour
{
    private GameObject bullet;
    private Vector3 rightTopPosition;

    private Vector2 bulletVelocity = new Vector2(0, 5);

    // Use this for initialization
    void Start()
    {
        this.GetComponent<Rigidbody2D>().velocity = bulletVelocity;
        this.bullet = gameObject;
        this.rightTopPosition = Camera.main.ViewportToWorldPoint(new Vector3(1, 1));
    }

    // Update is called once per frame
    void Update()
    {
        // a bullet is destroyed when she's out of screen
        if ( this.rightTopPosition.y < this.bullet.transform.position.y )
        {
            Destroy(bullet);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // when player's bullet hit an enemy's bullet
        // they're destroyed
        if ( collision.tag == Tags.ENEMY_BULLET )
        {
            Destroy(collision.gameObject);
            Destroy(gameObject);
        }
    }
}
