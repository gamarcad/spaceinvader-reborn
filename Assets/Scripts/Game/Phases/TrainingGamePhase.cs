﻿public class TrainingGamePhase : GamePhase
{
    private int currentWave;

    public TrainingGamePhase(GameManager gameManager) : base(gameManager, "TRAINING")
    {
    }

    public override void Start()
    {
        this.currentWave = 0;
        
    }

    public override void Stop()
    {
        
    }

    public override void Update()
    {
        if ( currentWave == 4 && this.allEnemiesKilled() )
        {
            this.finish();
        }
        else if (currentWave == 3 && this.allEnemiesKilled())
        {
            this.startFourWave();
        }
        else if (currentWave == 2 && this.allEnemiesKilled())
        {
            this.startThirdWave();
        }
        else if (currentWave == 1 && this.allEnemiesKilled())
        {
            startSecondWave();
        }
        else if (currentWave == 0)
        {
            this.startFirstWave();
        }
    }

    private void startFirstWave()
    {
        this.currentWave = 1;
        createEnemies(EnemyType.SIMPLE_ENEMY, 2);
    }

    private void startSecondWave()
    {
        this.currentWave = 2;
        this.createEnemies(EnemyType.SIMPLE_ENEMY, 2);
        this.createEnemies(EnemyType.DEFENSER_ENEMY, 1);
        this.createBonus(BonusType.LIFE_BONUS);
    }

    private void startThirdWave()
    {
        this.currentWave = 3;
        this.createEnemies(EnemyType.SIMPLE_ENEMY, 2);
        this.createEnemies(EnemyType.FIRE_ENEMY, 3);
        this.createBonus(BonusType.LIFE_BONUS);
    }

    private void startFourWave()
    {
        this.currentWave = 4;
        this.createEnemies(EnemyType.BOSS_ENEMY, 1);
        createBonus(BonusType.LIFE_BONUS);
        this.createBonus(BonusType.LIFE_BONUS);
        this.createBonus(BonusType.DOUBLE_FIRE_BONUS);
    }
}
