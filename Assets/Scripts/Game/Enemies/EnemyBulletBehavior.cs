﻿using UnityEngine;
using System.Collections;

public class EnemyBulletBehavior : MonoBehaviour
{
    // camero position
    private Vector3 leftBottomPosition;

    // bullet game object (this)
    private GameObject bullet;

    // Use this for initialization
    void Start()
    {
        this.leftBottomPosition = Camera.main.ViewportToWorldPoint(new Vector3(0, 0));

        this.bullet = gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        // destroy bullet when no enemy hit
        Vector3 bulletPosition = this.bullet.transform.position;
        if ( bulletPosition.y < this.leftBottomPosition.y )
        {
            Destroy(bullet);
        }
    }
}
