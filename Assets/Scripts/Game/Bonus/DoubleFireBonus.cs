﻿using UnityEngine;
using System.Collections;

public class DoubleFireBonus : AbstractBonusBehavior
{
    protected override void OnBonusClaimed()
    {
        ShipFire shipFire = GameObject.Find("ship").GetComponent<ShipFire>();
        shipFire.enableFireMode(ShipFire.FireMode.DOUBLE_FIRE);
    }
}
