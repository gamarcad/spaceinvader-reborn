﻿using UnityEngine;
using System.Collections;

public class ShipFire : MonoBehaviour
{
    private GameObject ship;
    private const int FRAME_FIRE_RANGE = 20;
    private int frameUntilFire;
    private AudioSource fireGunAudio;


    private FireMode fireMode;
    private int remaingBonusFire;

    // Use this for initialization
    void Start()
    {
        this.ship = gameObject;
        this.remaingBonusFire = 0;
        this.fireMode = FireMode.SIMPLE_FIRE;
        this.blockFire();
        this.fireGunAudio = GameObject.Find("FireSound").GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        this.frameUntilFire--;
        this.frameUntilFire = 0 <= this.frameUntilFire ? this.frameUntilFire : 0;
        // ship fire at top direction touch
        float moveVertical = Input.GetAxis("Vertical");
        if (0 < moveVertical)
        {
            if (this.fireEnabled())
            { 
                this.fire();
            }
        }
    }

    public void fire()
    {
        if (!this.fireEnabled())
        {
            return;
        }

        // prevent unlimited fire
        this.blockFire();

        // simple fire is default mode, just creates a bullet
        // and hopely shot an enemy
        if (this.fireMode == FireMode.SIMPLE_FIRE)
        {
            GameObject bullet = Instantiate(Resources.Load("Bullet"), this.ship.transform.position, Quaternion.identity) as GameObject;
            this.playFireGunSound();
            return;
        }

        // if fire mode is bonus mode
        // check if bonus mode is still available
        this.remaingBonusFire--;
        if (this.remaingBonusFire == 0)
        {
            this.fireMode = FireMode.SIMPLE_FIRE;
            return;
        }

        
        if (this.fireMode == FireMode.SUPER_FIRE)
        {
            GameObject bullet = Instantiate(Resources.Load("super_bullet"), this.ship.transform.position, Quaternion.identity) as GameObject;
            this.playFireGunSound();
        }

        if (this.fireMode == FireMode.DOUBLE_FIRE)
        {
            Vector3 shipPosition = this.ship.transform.position;
            Vector3 leftBulletPosition = new Vector3(shipPosition.x - 0.05f, shipPosition.y);
            GameObject bulletLeft = Instantiate(Resources.Load("Bullet"), leftBulletPosition, Quaternion.identity) as GameObject;

            Vector3 rightBulletPosition = new Vector3(shipPosition.x + 0.05f, shipPosition.y);
            GameObject bulletRight = Instantiate(Resources.Load("Bullet"), rightBulletPosition, Quaternion.identity) as GameObject;
            this.playFireGunSound();
            remaingBonusFire--;

            // if no remaining bonus shot, get back to simple fire
            if (this.remaingBonusFire == 0)
            {
                this.fireMode = FireMode.SIMPLE_FIRE;
            }
        }
    }

    public void enableFireMode( FireMode fireMode, int availableFire = 10 ) 
    {
        this.fireMode = fireMode;
        if ( fireMode == FireMode.SIMPLE_FIRE )
        {
            this.fireMode = 0;
        } else
        {
            this.remaingBonusFire = availableFire;
        }

    }

    private bool fireEnabled()
    {
        return this.frameUntilFire == 0;
    }

    private void blockFire()
    {
        this.frameUntilFire = FRAME_FIRE_RANGE;
    }


    private void playFireGunSound()
    {
        this.fireGunAudio.Play();
    }

    public enum FireMode { SIMPLE_FIRE, DOUBLE_FIRE, SUPER_FIRE }
}
