﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


/**
 * Enemies Manager allows to creates enemy.
 * Major feature is to prevent a "big" waves
 * by creating them in regular rate.
 */ 
public class EnemiesManager : MonoBehaviour, ScheduledTask
{

    private const int ENEMY_CREATION_FRAMES_RATE = 64;

    // awaiting enemies list
    private List<Object> awaitingEnemies;

    // camera position
    private Vector3 leftBottomPosition;
    private Vector3 rightTopPosition;
    private Vector3 hiddenPosition;

    // Use this for initialization
    void Start()
    {
        this.awaitingEnemies = new List<Object>();

        // init camera position
        this.leftBottomPosition = Camera.main.ViewportToWorldPoint(new Vector3(0, 0));
        this.rightTopPosition = Camera.main.ViewportToWorldPoint(new Vector3(1, 1));
        this.hiddenPosition = Camera.main.ViewportToWorldPoint(new Vector3(2, 2));

        // creates task scheduler
        TaskScheduler taskScheduler = TaskScheduler.GetInstance();
        taskScheduler.ScheduleTask(ENEMY_CREATION_FRAMES_RATE, this);
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    public void ExecuteTask()
    {
        // creates first awaiting enemy
        if(this.awaitingEnemies.Count != 0)
        {
            this.instantiateEnemy();
        }
    }

    /**
     * Returns displayed and scheduled enemy number
     */ 
    public int enemiesNumber()
    {
        int displayedEnemiesNumber = GameObject.FindGameObjectsWithTag("Enemy").Length;
        int scheduledEnemiesNumber = this.scheduledEnemies();
        return displayedEnemiesNumber + scheduledEnemiesNumber;
    }

    /*
     * Returns scheduled enemies.
     */ 
    public int scheduledEnemies()
    {
        return this.awaitingEnemies.Count;
    }

    /**
     * Creates a specified enemy:
     */ 
    public void createEnemies(EnemyType enemyType, int enemiesNumber)
    {
        switch (enemyType)
        {
            case EnemyType.DEFENSER_ENEMY:
                this.storeEnemies(enemiesNumber, "enemy_green");
                break;
            case EnemyType.FIRE_ENEMY:
                this.storeEnemies(enemiesNumber, "enemy_purple");
                break;
            case EnemyType.SIMPLE_ENEMY:
                this.storeEnemies(enemiesNumber, "enemy_blue");
                break;
            case EnemyType.BOSS_ENEMY:
                this.storeEnemies(1, "boss");
                break;
            default:
                Debug.LogError("Cannot instantiate undefined enemy");
                break;
        }
    }


    /**
     * Stores enemy to awaiting list.
     */
    private void storeEnemies( int enemiesNumber, string enemyName )
    {
        for (int i = 1; i <= enemiesNumber; ++i )
        {
            this.awaitingEnemies.Add(Resources.Load(enemyName));
        }
    }

    /**
     * Instantiates first awaiting enemy list.
     */
    private void instantiateEnemy()
    {
        Object enemy = this.awaitingEnemies[0];
        this.awaitingEnemies.Remove(enemy);
        Instantiate(enemy, createInitialCoordinates( enemy ), Quaternion.identity);
    }

    /**
     * Creates initial coordinates to spawn enemy.
     */
    private Vector3 createInitialCoordinates(Object enemy)
    {
        // create an instance of enemy to compute sprite size
        GameObject enemyInstance = Instantiate(enemy, hiddenPosition, Quaternion.identity) as GameObject;
        Vector2 spriteSize = enemyInstance.GetComponent<SpriteRenderer>().size;
        Destroy(enemyInstance);

        float offset = spriteSize.x / 2;
        float randomX = Random.Range(leftBottomPosition.x + offset, rightTopPosition.x - offset);
        return new Vector3(randomX, rightTopPosition.y + 0.5f);
    }
}
