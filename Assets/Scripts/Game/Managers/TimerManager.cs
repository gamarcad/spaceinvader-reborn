﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

/**
 * Manages game's time.
 */  
public class TimerManager : MonoBehaviour
{
    // game start datetime
    private DateTime startDateTime;

    // timer displaying
    private Text timerText;
    private TimeSpan lastOffset;

    // Use this for initialization
    void Start()
    {
        this.startDateTime = DateTime.Now;
        this.timerText = GameObject.Find("TimerText").GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        DateTime now = DateTime.Now;
        TimeSpan offset = now - startDateTime;
        this.lastOffset = offset;

        String text = formatInt( offset.Minutes, 2 ) + ":" +
            formatInt( offset.Seconds, 2 ) + ":" +
            formatInt( offset.Milliseconds, 3);

        this.timerText.text = text;
    }

    /**
     * Saves player's game time.
     */ 
    public void saveGameTime()
    {
        // add game's time into time storage.
        TimeStorage timeStorage = TimeStorage.GetInstance();
        timeStorage.AddTime(lastOffset);
    }


    /**
     * format int to get corrected length integer.
     */ 
    private String formatInt( int value, int minLength )
    {
        String valueStr = value.ToString();
        for ( int i = 1 + valueStr.Length; i <= minLength; ++i )
        {
            valueStr = "0" + valueStr;
        }
        return valueStr;
    }
}
